package com.reksa.karang.retrofitkotlin

data class Movie(
    val id : Int,
    val title: String,
    val year: String,
    val genre: String,
    val poster: String){}
