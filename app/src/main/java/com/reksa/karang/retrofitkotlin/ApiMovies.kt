package com.reksa.karang.retrofitkotlin

import retrofit2.http.GET
import rx.Observable


interface ApiMovies {

    @GET("filippella/Sample-Api-Files/master/json/movies-api.json")
    fun getMovies() : Observable<MovieResponse>

}